var code = {
    // Returns "Hello World!"
    helloWorld: function() {
        return 'Hello World!';
    },
 
    // Take a single-spaced <sentence>, and capitalize every <n>th word starting with <offset>.
    capitalizeEveryNthWord: function(sentence, offset, n) {
        String.prototype.capitalizeFirstLetter = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        };
 
        var wordsArray = sentence.split(' ');
        var indexForCapitalization = offset;
 
        while(indexForCapitalization < wordsArray.length) {
            wordsArray[indexForCapitalization] = wordsArray[indexForCapitalization].capitalizeFirstLetter();
            indexForCapitalization += n;
        }
 
        return wordsArray.join(' ');
    },
     
    // Determine if a number is prime
    isPrime: function(n) {
        if (n < 2) {
            return false;
        }
 
        // Check for integer
        if (n != Math.round(n)) {
            return false;
        }
 
        for (var i = 2; i < n; i++) {
            if (n % i === 0) {
                return false;
            }
        }
 
        return true;
    },
     
    // Calculate the golden ratio.
    // Given two numbers a and b with a > b > 0, the ratio is b / a.
    // Let c = a + b, then the ratio c / b is closer to the golden ratio.
    // Let d = b + c, then the ratio d / c is closer to the golden ratio. 
    // Let e = c + d, then the ratio e / d is closer to the golden ratio.
    // If you continue this process, the result will trend towards the golden ratio.
    goldenRatio: function(a, b) {
        var small = a
            , large = b
            , ratio = 0
            , decimals = 6;
 
        while (( large / small ).toFixed( decimals ) !== ratio.toFixed( decimals ) ) {
            var tempSmall = large;
            ratio = large/small;
            large += small;
            small = tempSmall;
        }
 
        return ratio;
    },
 
    // Give the nth Fibonacci number
    // Starting with 0, 1, 1, 2, ... a Fibonacci number is the sum of the previous two.
    fibonacci: function(n) {
        var fibonacciSequence = [0,1];
 
        for (var i = 0; i < n; i++) {
            fibonacciSequence.push(fibonacciSequence.slice(-1)[0] + fibonacciSequence.slice(-2)[0]);
        }
 
        return fibonacciSequence[n];
    },
 
    // Give the square root of a number
    // Using a binary search algorithm, search for the square root of a given number.
    // Do not use the built-in square root function.
    squareRoot: function(n) {
        var low = 0
            , high = n+1
            , mid
            ;

        while (high - low > 0.0001) {
            mid = (low + high) / 2;
            if (mid * mid <= n) {
                low = mid;
            } else {
                high = mid;
            }
        }

        return parseFloat(low.toFixed(4));
    }
};
module.exports = code;